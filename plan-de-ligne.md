Page référençant les icônes pour les plans de lignes. Lors de l'affichage, le fond et le point sont superposés. Dans les schémas ci-dessous, on suppose que le texte est au centre de l'icône. Ainsi, pour le fond = 11 et point = 01, on obtiendra :
```
[ | ]
[ * ] Station
[ | ]
```

Fonds
=====
Les fonds sont disponible et suivent la logique suivante :

```
d↓\u→ 0     1     2     3     4
---------------------------------
    [XXX] [ | ] [|  ] [  |] [|_|]
 0  [XXX] [   ] [   ] [   ] [   ]
    [XXX] [   ] [   ] [   ] [   ]
---------------------------------
    [   ] [ | ] [XXX] [XXX] [|_|]
 1  [   ] [ | ] [XXX] [XXX] [ | ]
    [ | ] [ | ] [XXX] [XXX] [ | ]
---------------------------------
    [   ] [XXX] [|  ] [XXX] [| |]
 2  [   ] [XXX] [|  ] [XXX] [|  ]
    [|  ] [XXX] [|  ] [XXX] [|  ]
---------------------------------
    [   ] [XXX] [XXX] [  |] [| |]
 3  [   ] [XXX] [XXX] [  |] [  |]
    [  |] [XXX] [XXX] [  |] [  |]
---------------------------------
    [   ] [ | ] [|  ] [  |] [| |]
 4  [ _ ] [ ⊥ ] [|  ] [  |] [| |]
    [| |] [| |] [| |] [| |] [| |]
```

Ainsi, le fond = 42 correspond à : 
```
[|  ]
[|  ]
[| |]
```

NB : Les cases désignées par des XXX n'existent pas dans le système.

Points
======
De façon similaire, voici les différents points disponibles :
```
d↓\u→ 1     2     3
---------------------
    [   ] [   ] [   ]
 0  [ * ] [↓* ] [ *↑]
    [   ] [   ] [   ]
---------------------
    [   ] [   ] [   ]
 1  [  *] [ ↓*] [ ↑*]
    [   ] [   ] [   ]
---------------------
    [   ] [   ] [   ]
 2  [*  ] [*↓ ] [*↑ ]
    [   ] [   ] [   ]
```