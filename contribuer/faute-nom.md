L'application utilise un numéro de version, qui lui permet de savoir quand il doit se mettre à jour ou non. Pour corriger un nom, il suffit alors de créer une nouvelle révision pour que le processus de mise à jour se mette en route

Créer une nouvelle révision
--------------------------------------------
* Ouvrez le fichier app/src/main/java/fr/nocle/passegares/controlleur/Controlleur.java
* Repérer la ligne :

```java
protected final static int VERSION = 105;
```

* Ici, l'application est en version 105. Incrémentons ce nombre de 1. Cela va devenir le numéro de révision que l'on va créer. Dans notre cas, ce sera 106. Retenez ce nombre.

Corriger les fautes
-----------------------------
* Les fichiers contenant les données se trouvent dans le dossier app/src/main/assets Dedans, chaque région est représenté par un dossier. Par exemple, disons que le problème se situe dans le dossier Paris. Ouvrons le.
* Chaque dossier comporte trois fichiers. Les plus intéressants sont [Gares.csv](/Gares.csv) qui contient la liste des gares, et [Lignes.csv](/Lignes.csv) qui contient la liste des lignes de la région. En fonction du type d'erreur, ouvrez le dossier correspondant. Dans notre exemple, ouvrons Gares.csv. Comme il s'agit de tableaux, cela s'ouvre avec n'importe quel tableur.
* Deux colonnes vont nous intéresser : la colonne nom, qui sera là où se trouve le nom des gares et des lignes, et est donc là où se trouve la faute à corriger, et la colonne vMaj. Cette dernière colonne permet d'indiquer la dernière révision où une information de la ligne a été modifié. C'est comme ça que le système détecte les lignes à mettre à jour. Comme nous venons de la modifier, nous allons donc la mettre à jour. Dans notre exemple, nous allons donc mettre 106 dans cette cellule.
* Il est possible de modifier plusieurs noms en même temps, à condition de mettre à jour à chaque fois la cellule vMaj.
* Attention, si la gare se trouve dans plusieurs régions (c'est le cas par exemple pour la gare d'Orry-la-Ville, qui se trouve à la fois dans Hauts de France (position géographique) mais aussi dans le dossier pour l'Île-de-France (car desservie par le RER)), il faut mettre à jour de façon identique chaque fichier concerné.

Mettre à jour le fichier Regions.csv
--------------------------------------------------------
* Maintenant que nous avons modifié des fichiers, il reste un dernier fichier à modifier. En effet, comme chaque utilisateur n'a pas forcement toutes les régions possibles installés, un fichier regroupe les versions de dernières modifications pour chaque dossier. Il s'agit de Regions.csv
* Ouvrez app/src/main/assets/Regions.csv. Pour chaque région contenant au moins un fichier modifié, il faut mettre à jour la cellule vMaj correspondante avec ce nombre

Tester la cohérence
-------------------------------
* Votre travail est terminé, il reste maintenant à vérifier que tout est bon, et que rien n'a été oublié. Pour cela, un outil existe pour tester la cohérence de données. Il se lance automatiquement à chaque commit, mais vous pouvez le lancer directement sur votre machine pour vérifier que tout est bon.
* Il s'agit du script  testeur/regions/testerCoherenceDonnees.py C'est un programme python.
* Lancez-le. En cas de problème, un fichier rapport.txt est généré dans un sous dossier rapport/. Il va contenir toutes les incohérences, et donc vous demander de les corriger.
* Il est nécessaire que tout soit corriger avant de proposer un merge request, sous peine de rejet de vos modifications.