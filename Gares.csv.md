Lien vers le fichier : https://framagit.org/JonathanMM/passegares/blob/master/app/src/main/assets/Paris/Gares.csv

Description des colonnes :
* *idExterne* :	Identifiant de la gare (Texte)
* *nom* : Nom de la gare (Texte)
* *exploitant* : Société(s) exploitante(s) (Texte)
* *latitude* : Latitude Nord en degrés décimaux (Nombre décimal)
* *longitude* :	Longitude Est en degrés décimaux (Nombre décimal)
* *couleur* : Couleur de la gare (0 à 7)
* *couleurEvolution* : Couleur pour faire monter de niveau la gare (0 à 7)
* *vCreation* : Version de création de la gare (Nombre)
* *vMaj* : Version de dernière MàJ de la gare (Nombre)
* *vSuppression* : Version de suppression de la gare (Nombre)