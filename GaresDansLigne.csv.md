Lien vers le fichier : https://framagit.org/JonathanMM/passegares/blob/master/app/src/main/assets/Paris/GaresDansLigne.csv

Colonnes :
* **idGare** : Identifiant de la gare dans le fichier Gares.csv, colonne idStif
* **idLigne** : Identifiant de la ligne dans le fichier Lignes.csv, colonne idStif
* **ordre** : Ordre de la gare dans la ligne. Doit être dans l'ordre croissant, le 1 correspondant à au terminus le plus au nord ouest de la ligne   
* **PDLFond** : Icône de fond dans le plan de ligne (cf [Plan de ligne](plan-de-ligne))
* **PDLPoint** : Icône du point dans le plan de ligne (cf [Plan de ligne](plan-de-ligne))
* **vCreation** : Version de création de l'enregistrement
* **vMaj** : Version de la dernière mise à jour de l'enregistrement
* **vSuppression** : Version de suppression de l'enregistrement. Si différent de 0, alors vCreation et vMaj doivent être à 0.