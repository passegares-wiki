Lien vers le fichier : https://framagit.org/JonathanMM/passegares/blob/master/app/src/main/assets/Paris/Lignes.csv

* *idExterne* :	Identifiant de la ligne (Texte)
* *nom* : Nom de la ligne (Texte)
* *type* : Type de la ligne (Métro, RER, Train, Navette, Tramway)
* *ordre* : Ordre de la ligne (Nombre entier)
* *couleur* : Couleur principal de la ligne, pour l'affichage de son plan de ligne (code hexadécimal de la couleur)
* *vCreation* : Version de création de la gare (Nombre)
* *vMaj* : Version de dernière MàJ de la gare (Nombre)
* *vSuppression* : Version de suppression de la gare (Nombre)