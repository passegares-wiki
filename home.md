Format des fichiers de données :
* [Gares.csv](Gares.csv)
* [GaresDansLigne.csv](GaresDansLigne.csv)
* [Lignes.csv](Lignes.csv)

Liste des ressources :
* [Plan de ligne](plan-de-ligne)

Contribuer :
* [Corriger une faute dans un nom de gare](contribuer/faute-nom)